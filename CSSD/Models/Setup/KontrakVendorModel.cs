﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Setup
{
    public class KontrakVendorModel
    {
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Nama_Supplier { get; set; }
        public string Kode_Item { get; set; }
        public string Nama_Item { get; set; }
        public int? Barang_ID { get; set; }
        public short? SupplierID { get; set; }
        public short? Supplier_ID { get; set; }
        public string Kode_Supplier { get; set; }
        public string Supplier_Nama_Supplier { get; set; }
        public decimal? Harga { get; set; }

    }
}