﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Transaksi
{
    public class MutasiLinenKeSectionModel
    {
        public DateTime Tanggal { get; set; }
        public int MutasiDari { get; set; }
        public string NoAmprahan { get; set; }
        public string Keterangan { get; set; }
        public List<MutasiDetailLinenKeSectionModel> Detail { get; set; }
    }

    public class MutasiDetailLinenKeSectionModel
    {
        public int Id { get; set; }
        public int Qty { get; set; }
    }
}