﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using CSSD.Models.Transaksi;
using System.Security.Cryptography.X509Certificates;
using System.Web.ModelBinding;

namespace CSSD.Controllers.Transaksi
{
    public class LinenKotorInternalController : Controller
    {
        // GET: LinenKotorInternal
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListLinenKotorInternal.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi <= d);
                        }
                        else if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.No_Bukti.Contains(x.Value) ||  
                                y.LokasiAsal_Nama_Lokasi.Contains(x.Value) ||
                                y.LokasiTujuan_Nama_Lokasi.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value)
                               
                            );

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        No_Bukti = x.No_Bukti,
                        Tgl_Mutasi = x.Tgl_Mutasi.ToString("dd/MM/yyyy"),
                        Keterangan = x.Keterangan ?? "",
                        LokasiAsal_Nama_Lokasi = x.LokasiAsal_Nama_Lokasi,
                        LokasiTujuan_Nama_Lokasi = x.LokasiTujuan_Nama_Lokasi,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, LinenKotorInternalModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<LinenKotorInternalModelDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.CSSD_InsertHeaderLinenKotorInternal(
                                model.Lokasi_Asal,
                                model.Tgl_Mutasi,
                                model.Keterangan
                            ).FirstOrDefault();

                            foreach (var x in model.Detail)
                            {
                               
                                s.CSSD_InsertDetailLinenKotorInternal(
                                    id,
                                    x.Barang_ID,
                                    x.Qty,
                                    model.Lokasi_Asal
                                );
                            }
                            s.SaveChanges();
                        }

                       

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"LinenKotorInternal-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_ListLinenKotorInternal.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.CSSD_GetDetailLinenKotorInternal.Where(x => x.No_Bukti == m.No_Bukti).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No_Bukti = m.No_Bukti,
                            Tgl_Mutasi = m.Tgl_Mutasi.ToString("yyyy-MM-dd"),
                            Nama_Lokasi = m.LokasiTujuan_Nama_Lokasi,
                            Lokasi_Asal = m.LokasiAsal_Nama_Lokasi,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            No_Bukti = x.No_Bukti,
                            Kode_Barang = x.Kode_Barang,
                            Nama_Barang = x.Nama_Barang,
                            Kode_Satuan = x.Kode_Satuan,
                            Qty = x.Qty,
                            Harga = x.Harga,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupPasien(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    
                    var proses = s.CSSD_GetDataLaundryLinenKotorInternal.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Barang_ID =  x.Barang_ID,
                        Kode_Barang = x.Kode_Barang,
                        Nama_Barang = x.Nama_Barang,
                        Kode_Satuan = x.Kode_Satuan_Beli,
                        Harga = x.Harga_Jual, 
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}


//else if (_process == "EDIT")
//{
//    if (model.Detail == null) model.Detail = new List<OtherPaketObatDetailModel>();

    //    s.ADM_UpdatePaketObat(
    //        model.Id,
    //        model.Nama,
    //        model.Section,
    //        DateTime.Today,
    //        model.Dokter,
    //        model.ObatPuyer,
    //        model.Keterangan
    //    );

    //    var d = s.ADM_GetDetailPaketObat.Where(x => x.KodePaket == model.Id).ToList();
    //    foreach (var x in d)
    //    {
    //        var _d = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
    //        //delete
    //        if (_d == null) s.ADM_DeleteDetailPaketObat(model.Id, x.Barang_ID);
    //    }
    //    foreach (var x in model.Detail)
    //    {
    //        var _d = d.FirstOrDefault(y => y.Barang_ID == x.Id);
    //        if (_d == null)
    //        {
    //            // new
    //            s.ADM_InsertDetailPaketObat(
    //                model.Id,
    //                x.Id,
    //                x.Qty,
    //                x.AturanPakai
    //            );
    //        }
    //        else
    //        {
    //            // edit
    //            s.ADM_UpdateDetailPaketObat(
    //                model.Id,
    //                x.Id,
    //                x.Qty,
    //                x.AturanPakai
    //            );
    //        }

    //    }
    //    s.SaveChanges();
    //}
//else if (_process == "DELETE")
//{
//    var d = s.CSSD_GetDetailLinenKotorPasien.Where(x => x.No_Bukti == model.No_Bukti.ToList();
//    foreach (var x in d)
//    {
//        s.CSSD_DeleteDetailLinenKotorPasien(x.No_Bukti);
//    }
//    s.CSSD_DeleteLinenKotorPasien(x.Alasan_Batal, x.No_Bukti);
//    s.SaveChanges();
//}