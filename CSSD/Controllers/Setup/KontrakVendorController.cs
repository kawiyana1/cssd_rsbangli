﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using CSSD.Models.Setup;

namespace CSSD.Controllers.Setup
{
    public class KontrakVendorController : Controller
    {
        // GET: KontrakVendor
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListSetupKontrak.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Kode_Item.Contains(x.Value) || 
                                                       y.Nama_Item.Contains(x.Value) ||
                                                       y.Kode_Supplier.Contains(x.Value) ||
                                                       y.Supplier_Nama_Supplier.Contains(x.Value) 
                                                       );

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Kode_Item = x.Kode_Item,
                        Nama_Item = x.Nama_Item,
                        Kode_Supplier = x.Kode_Supplier,
                        Supplier_Nama_Supplier = x.Supplier_Nama_Supplier,
                        Harga = x.Harga,

                    }); ;
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, KontrakVendorModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.CSSD_InsertSetupKontrak(model.SupplierID, model.Barang_ID, model.Harga);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.CSSD_UpdateSetupKontrak(model.SupplierID, model.Barang_ID, model.Harga);
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.CSSD_DeleteSetupKontrak(model.SupplierID, model.Barang_ID, model.Harga);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupKontrakVendor-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_ListSetupKontrak.FirstOrDefault(x => x.Kode_Item == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            SupplierID = m.SupplierID,
                            Barang_ID = m.Barang_ID,
                            Kode_Barang = m.Kode_Item,
                            Nama_Barang = m.Nama_Item,
                            Kode_Supplier = m.Kode_Supplier,
                            Nama_Supplier = m.Supplier_Nama_Supplier,
                            Harga = m.Harga,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P 

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetBarangSetupKontrak.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Barang_ID = x.Barang_ID,
                        Kode_Barang = x.Kode_Barang,
                        Nama_Barang = x.Nama_Barang,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        public string ListLookupSupplier(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetSupplierSetupKontrak.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Supplier.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        SupplierID = x.Supplier_ID,
                        Kode_Supplier = x.Kode_Supplier,
                        Nama_Supplier = x.Nama_Supplier,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}